package io.codeherald.kotlin.snake

import com.googlecode.lanterna.terminal.DefaultTerminalFactory
import com.googlecode.lanterna.terminal.Terminal
import java.util.*

enum class Direction{
    LEFT, RIGHT, UP, DOWN
}

class Point(val rows: Int, val columns: Int)

open class Node(val point:Point){
    open var char = ' '
}

class Wall(point:Point): Node(point) {
    override var char = 'W'
}

class Snake(point:Point, direction: Direction): Node(point) {
    override var char = '@'
}

class Fruit(point:Point): Node(point) {
    override var char = 'F'
}

class Board(terminal: Terminal) {
    var terminal:Terminal? = terminal

    var walls = ArrayList<Wall>()
    var fruits = ArrayList<Fruit>()
    var snake = Stack<Snake>()

    var rows: Int = 0
    var columns: Int = 0

    init {
        this.terminal!!.setCursorVisible(false)
        rows = terminal.terminalSize.rows
        columns = terminal.terminalSize.columns
        this.walls = instWalls(rows, columns)
        this.snake.add(Snake(Point(15,15), Direction.RIGHT))
        this.fruits.add(Fruit(Point(15, 18)))
        this.fruits.add(Fruit(Point(7, 6)))
    }

    fun instWalls(rows:Int, columns:Int): ArrayList<Wall> {
        val createdWalls = ArrayList<Wall>()
        for (i in 2..rows-2) {
            createdWalls.add(Wall(Point(i, columns - 1)))
            createdWalls.add(Wall(Point(i, 0)))
        }
        for (i in 0..columns-1) {
            createdWalls.add(Wall(Point(1, i)))
            createdWalls.add(Wall(Point(rows - 1, i)))
        }
        return createdWalls
    }

    fun drawNode(node:Node){
        terminal?.setCursorPosition(node.point.columns, node.point.rows)
        terminal?.putCharacter(node.char)
    }

    fun draw(terminal: Terminal){
        terminal.clearScreen()
        for(node in walls.union(snake).union(fruits)){
            drawNode(node)
        }
        terminal.flush()
    }

}

fun main(args: Array<String>){
    //create and present our Terminal window
    val terminal: Terminal = DefaultTerminalFactory().createTerminal()

    //initialize the board
    var board: Board = Board(terminal)

    //kick off the game loop
    while (true){
        board.draw(terminal)
        Thread.sleep(1000)
    }
}
