package io.codeherald.kotlin.snake2

import com.googlecode.lanterna.TextColor
import com.googlecode.lanterna.graphics.TextGraphics
import com.googlecode.lanterna.input.KeyType
import com.googlecode.lanterna.terminal.DefaultTerminalFactory
import com.googlecode.lanterna.terminal.Terminal
import java.security.SecureRandom
import java.util.*

enum class Direction{
    LEFT, RIGHT, UP, DOWN, NA
}
enum class Collision{
    NO, EAT, DIE
}

data class Point(val rows: Int, val columns: Int){
    fun pointFor(deltaRows:Int, deltaCols:Int): Point{
        return Point(rows+deltaRows, columns+deltaCols)
    }
}

open class Node(val point: Point){
    open var char = ' '
    open var backgroundColor: TextColor = TextColor.ANSI.BLACK
    open var foregroundColor: TextColor = TextColor.ANSI.WHITE
}

class Wall(point: Point): Node(point) {
    override var backgroundColor: TextColor = TextColor.ANSI.WHITE
    override var foregroundColor: TextColor = TextColor.ANSI.WHITE
    override var char = ' '
}

open class Snake(point: Point, var direction: Direction): Node(point) {
    override var char = ' '
    override var backgroundColor: TextColor = TextColor.ANSI.WHITE
    override var foregroundColor: TextColor = TextColor.ANSI.WHITE
}

class DeadSnake(point: Point, direction: Direction) : Snake(point, direction){
    override var char = 'X'
    override var backgroundColor: TextColor = TextColor.ANSI.RED
    override var foregroundColor: TextColor = TextColor.ANSI.WHITE
}

class Fruit(point:Point): Node(point) {
    override var char = 'F'
    var visible = true
}

class FruitTimerTask(var fruit:Fruit): TimerTask(){
    override fun run() {
        fruit.visible = false
    }
}

class Board {
    var terminal:Terminal? = null
    var walls = ArrayList<Wall>()
    var fruits = ArrayList<Fruit>()
    var snake = Stack<Snake>()
    var score: Int = 0
    var rows: Int = 0
    var columns: Int = 0

    constructor(terminal: Terminal){
        this.terminal = terminal
        this.terminal!!.setCursorVisible(false)

        //fetch size of the terminal window, which will determine our board size
        rows = terminal.terminalSize.rows
        columns = terminal.terminalSize.columns

        this.walls = instWalls(rows, columns)

        this.snake.add(Snake(Point(15,15), Direction.RIGHT))
    }

    fun instWalls(rows:Int, columns:Int): ArrayList<Wall> {
        val createdWalls = ArrayList<Wall>()
        for (i in 2..rows-2) {
            createdWalls.add(Wall(Point(i, columns - 1)))
            createdWalls.add(Wall(Point(i, 0)))
        }
        for (i in 0..columns-1) {
            createdWalls.add(Wall(Point(1, i)))
            createdWalls.add(Wall(Point(rows - 1, i)))
        }
        return createdWalls
    }

    fun addRandomFruit(){
        val random = SecureRandom()
        val random_column = random.nextInt(columns-2) + 1
        val random_row = random.nextInt(rows-3) + 2
        val newFruit: Fruit = Fruit(Point(random_row, random_column))
        fruits.add(newFruit)
        Timer().schedule(FruitTimerTask(newFruit), 10000L)
    }

    fun cleanInvisibleFruits(){
        this.fruits.filter { it.visible==false }.toList().forEach( { fruits.remove(it) })
    }

    fun presentScore(){
        var textTerminalGraphics: TextGraphics? = terminal?.newTextGraphics()
        textTerminalGraphics?.putString(0,0,"Score: $score")
    }

    fun drawNode(node:Node){
        terminal?.setCursorPosition(node.point.columns, node.point.rows)
        terminal?.setBackgroundColor(node.backgroundColor)
        terminal?.setForegroundColor(node.foregroundColor)
        terminal?.putCharacter(node.char)
    }

    fun draw(){
        terminal?.clearScreen()
        presentScore()
        for(node in walls.union(snake).union(fruits)){
           drawNode(node)
        }
        terminal?.flush()
    }

    fun updateSnakeDirection(){
        when(terminal?.pollInput()?.keyType) {
            KeyType.ArrowLeft -> snake.peek().direction = Direction.LEFT
            KeyType.ArrowRight -> snake.peek().direction = Direction.RIGHT
            KeyType.ArrowUp -> snake.peek().direction = Direction.UP
            KeyType.ArrowDown -> snake.peek().direction = Direction.DOWN
        }
    }

    fun nextPointInDirection(origin:Point, direction:Direction): Point{
        var deltaRows = 0
        var deltaCols = 0
        when(direction){
            Direction.LEFT  -> deltaCols = -1
            Direction.RIGHT -> deltaCols = 1
            Direction.UP    -> deltaRows = -1
            Direction.DOWN  -> deltaRows = 1
            Direction.NA    -> {
                deltaCols = 0
                deltaRows = 0
            }
        }
        return origin.pointFor(deltaRows, deltaCols)
    }

    fun moveSnake(){
        val snakeHead: Snake = snake.peek()
        val nextHeadPoint: Point = nextPointInDirection(snakeHead.point, snakeHead.direction)
        snake.push(Snake(nextHeadPoint, snakeHead.direction))
        snake.remove(snake.firstElement())
        snakeHead.direction = Direction.NA
    }

    fun detectCollision(): Collision{
        val snakeHead: Snake = snake.peek()
        val targetPoint = nextPointInDirection(snakeHead.point, snakeHead.direction)
        //eaten a fruit?
        var fruitToEat: Fruit? = fruits.find { it.point == targetPoint }
        if(fruitToEat!=null){
            snake.push(Snake(fruitToEat.point,snakeHead.direction))
            snakeHead.direction = Direction.NA
            fruits.remove(fruitToEat)
            score++
            return Collision.EAT
        }
        //bumped into wall?
        var wallToHit: Wall? = walls.find { it.point == targetPoint }
        if(wallToHit!=null){
            snake.push(DeadSnake(wallToHit.point, Direction.NA))
            snakeHead.direction = Direction.NA
            return Collision.DIE
        }
        return Collision.NO
    }

}

fun main(args: Array<String>){
    //create and present our Terminal window
    val terminal: Terminal = DefaultTerminalFactory().createTerminal()
    //initialize the board
    var board: Board = Board(terminal)

    //kick off the game loop
    loop@ while (true){
        board.updateSnakeDirection()
        board.cleanInvisibleFruits()
        if(board.fruits.count { it.visible == true } <= 1)
            board.addRandomFruit()

        when(board.detectCollision()){
            Collision.EAT -> println("yum")
            Collision.DIE -> {
                println("oh no! game over")
                board.draw()
                break@loop
            }
            Collision.NO -> {
                print(".")
                board.moveSnake()
            }
        }
        board.draw()
        Thread.sleep(200)
    }
}
